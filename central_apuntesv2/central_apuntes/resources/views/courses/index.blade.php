<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pagina Principal de Cursos</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>CODIGO</th>
        <th>NOMBRE</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($courses as $course)

      <tr>
        <td>{{$course['pk']}}</td>
        <td>{{$course['code']}}</td>
        <td>{{$course['name']}}</td>


        <td><a href="{{action('CourseController@edit', $course['pk'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('CourseController@destroy', $course['pk'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>
