<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Pagina Principal de Cursos</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>TIPO DE DOCUMENTO</th>
        <th>CREADOR</th>
        <th>CURSO</th>
        <th>CODIGO</th>
        <th>RUTA</th>
        <th>NOMBRE</th>
        <th>FECHA CREACION</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($documents as $document)

      <tr>
        <td>{{$document['pk']}}</td>
        <td>{{$document['code']}}</td>
        <td>{{$document['name']}}</td>


        <td><a href="{{action('DocumentController@edit', $document['pk'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('DocumentController@destroy', $document['pk'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>
