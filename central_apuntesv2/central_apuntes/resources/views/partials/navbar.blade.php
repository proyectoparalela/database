
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/home">Panel de navegacion</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="/home">Volver al Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Usuarios
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="users/create">Crear Usuarios</a>
                        <a class="dropdown-item" href="users">Mostrar/Editar Usuarios</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="/home">back to home</a>
                      </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="courses" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Cursos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="courses">Crear Cursos</a>
                          <a class="dropdown-item" href="courses/create">Mostrar/Editar Cursos</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="/home">back to home</a>
                        </div>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="documents" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Documentos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="documents">Crear Documentos</a>
                          <a class="dropdown-item" href="documents/create">Mostrar/Editar Documentos</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="/home">back to home</a>
                        </div>
                      </li>
                  </ul>
                </div>
              </nav>
              {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> --}}


{{-- <div class="container-fluid" class="fas fa-align-center  ">
    <div class="btn-group">
        <button type="button" class="btn btn-danger">Usuarios</button>
        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="users/create">Crear Usuarios</a>
          <a class="dropdown-item" href="users">Mostrar/Editar Usuarios</a>
        </div>
      </div>
      <div class="btn-group">
        <button type="button" class="btn btn-danger">Cursos</button>
        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="courses">Mostrar/Editar Cursos</a>
          <a class="dropdown-item" href="courses/">Crear Cursos </a>
        </div>
      </div>
      <div class="btn-group">
        <button type="button" class="btn btn-danger">Documentos</button>
        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="documents">Mostrar/Editar Documentos</a>
          <a class="dropdown-item" href="documents/create"> Crear Documentos</a>
        </div>
      </div>
      <div class="btn-group">
        <button type="button" class="btn btn-danger">Tipos de Documentos</button>
        <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="documentstypes">Mostrar Tipos de Documentos</a>
          <a class="dropdown-item" href="documentstypes/create">Crear un Tipo de Documentos</a>
        </div>
      </div>
</div>
 --}}
