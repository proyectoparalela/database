<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    public $timestamps = false;
    protected $table = ('documents_types');
    protected $fillable = ['name','description'];

}
