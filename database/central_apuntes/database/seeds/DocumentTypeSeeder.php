<?php

use \App\Models\DocumentType;

use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $timestamps =false;

    public function run()
    {                  # code...
        for ($i=0; $i <10 ; $i++) {
            DocumentType::create([
                'name' => str_random(10),
                'description' => str_random(10),
            ]);
        }
    }
}
