<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->foreign('type_fk', 'documents_type_fk_fkey')->references('pk')->on('documents_types')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('owner_fk', 'documents_owner_fk_fkey')->references('pk')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('course_fk', 'documents_course_fk_fkey')->references('pk')->on('courses')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documents', function(Blueprint $table)
		{
			$table->dropForeign('documents_type_fk_fkey');
			$table->dropForeign('documents_owner_fk_fkey');
			$table->dropForeign('documents_course_fk_fkey');
		});
	}

}
