CREATE TABLE public."users "
(
    id_user bigint NOT NULL DEFAULT nextval('"users _id_user_seq"'::regclass),
    "name " character(255)[] COLLATE pg_catalog."default" NOT NULL,
    email character(255)[] COLLATE pg_catalog."default" NOT NULL,
    "user_rol " bigint NOT NULL,
    CONSTRAINT "users _pkey" PRIMARY KEY (id_user),
    CONSTRAINT email UNIQUE (email),
    CONSTRAINT "users _user_rol _fkey" FOREIGN KEY ("user_rol ")
        REFERENCES public.users_rol (rol) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public."users "
    OWNER to hector;
