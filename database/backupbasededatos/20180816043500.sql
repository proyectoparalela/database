--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acces ; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."acces " (
    "id_acces " integer NOT NULL,
    id_user integer NOT NULL,
    id_curso integer NOT NULL,
    date_time timestamp without time zone NOT NULL
);


ALTER TABLE public."acces " OWNER TO hector;

--
-- Name: acces _id_acces _seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."acces _id_acces _seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."acces _id_acces _seq" OWNER TO hector;

--
-- Name: acces _id_acces _seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."acces _id_acces _seq" OWNED BY public."acces "."id_acces ";


--
-- Name: curso; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.curso (
    id_curso integer NOT NULL,
    curso_name "char" NOT NULL,
    "description " "char"
);


ALTER TABLE public.curso OWNER TO hector;

--
-- Name: curso_id_curso_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.curso_id_curso_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.curso_id_curso_seq OWNER TO hector;

--
-- Name: curso_id_curso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.curso_id_curso_seq OWNED BY public.curso.id_curso;


--
-- Name: document; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.document (
    document_id bigint NOT NULL,
    document_name "char" NOT NULL,
    type_id integer NOT NULL
);


ALTER TABLE public.document OWNER TO hector;

--
-- Name: document_document_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.document_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_document_id_seq OWNER TO hector;

--
-- Name: document_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.document_document_id_seq OWNED BY public.document.document_id;


--
-- Name: storage_control ; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."storage_control " (
    storage_id bigint NOT NULL,
    id_curso integer NOT NULL,
    document_id integer NOT NULL,
    date_time_control timestamp without time zone NOT NULL
);


ALTER TABLE public."storage_control " OWNER TO hector;

--
-- Name: storage_control _storage_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."storage_control _storage_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."storage_control _storage_id_seq" OWNER TO hector;

--
-- Name: storage_control _storage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."storage_control _storage_id_seq" OWNED BY public."storage_control ".storage_id;


--
-- Name: type_doc; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.type_doc (
    type_id integer NOT NULL,
    type_name "char" NOT NULL
);


ALTER TABLE public.type_doc OWNER TO hector;

--
-- Name: type_doc_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.type_doc_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_doc_type_id_seq OWNER TO hector;

--
-- Name: type_doc_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.type_doc_type_id_seq OWNED BY public.type_doc.type_id;


--
-- Name: type_user; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.type_user (
    id_type integer NOT NULL,
    type_name "char" NOT NULL
);


ALTER TABLE public.type_user OWNER TO hector;

--
-- Name: TABLE type_user; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.type_user IS 'Tipo de usuario ';


--
-- Name: type_user_id_type_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.type_user_id_type_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_user_id_type_seq OWNER TO hector;

--
-- Name: type_user_id_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.type_user_id_type_seq OWNED BY public.type_user.id_type;


--
-- Name: user; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."user" (
    name "char" NOT NULL,
    email "char" NOT NULL,
    type_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public."user" OWNER TO hector;

--
-- Name: TABLE "user"; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public."user" IS 'usuarios ';


--
-- Name: user_type_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.user_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_type_id_seq OWNER TO hector;

--
-- Name: user_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.user_type_id_seq OWNED BY public."user".type_id;


--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_id_seq OWNER TO hector;

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.user_user_id_seq OWNED BY public."user".user_id;


--
-- Name: acces  id_acces ; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."acces " ALTER COLUMN "id_acces " SET DEFAULT nextval('public."acces _id_acces _seq"'::regclass);


--
-- Name: curso id_curso; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.curso ALTER COLUMN id_curso SET DEFAULT nextval('public.curso_id_curso_seq'::regclass);


--
-- Name: document document_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document ALTER COLUMN document_id SET DEFAULT nextval('public.document_document_id_seq'::regclass);


--
-- Name: storage_control  storage_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."storage_control " ALTER COLUMN storage_id SET DEFAULT nextval('public."storage_control _storage_id_seq"'::regclass);


--
-- Name: type_doc type_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.type_doc ALTER COLUMN type_id SET DEFAULT nextval('public.type_doc_type_id_seq'::regclass);


--
-- Name: type_user id_type; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.type_user ALTER COLUMN id_type SET DEFAULT nextval('public.type_user_id_type_seq'::regclass);


--
-- Name: user type_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user" ALTER COLUMN type_id SET DEFAULT nextval('public.user_type_id_seq'::regclass);


--
-- Name: user user_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user" ALTER COLUMN user_id SET DEFAULT nextval('public.user_user_id_seq'::regclass);


--
-- Data for Name: acces ; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."acces " ("id_acces ", id_user, id_curso, date_time) FROM stdin;
\.


--
-- Data for Name: curso; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.curso (id_curso, curso_name, "description ") FROM stdin;
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.document (document_id, document_name, type_id) FROM stdin;
\.


--
-- Data for Name: storage_control ; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."storage_control " (storage_id, id_curso, document_id, date_time_control) FROM stdin;
\.


--
-- Data for Name: type_doc; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.type_doc (type_id, type_name) FROM stdin;
\.


--
-- Data for Name: type_user; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.type_user (id_type, type_name) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."user" (name, email, type_id, user_id) FROM stdin;
\.


--
-- Name: acces _id_acces _seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."acces _id_acces _seq"', 1, false);


--
-- Name: curso_id_curso_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.curso_id_curso_seq', 1, false);


--
-- Name: document_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.document_document_id_seq', 1, false);


--
-- Name: storage_control _storage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."storage_control _storage_id_seq"', 1, false);


--
-- Name: type_doc_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.type_doc_type_id_seq', 1, false);


--
-- Name: type_user_id_type_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.type_user_id_type_seq', 1, false);


--
-- Name: user_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.user_type_id_seq', 1, false);


--
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.user_user_id_seq', 1, false);


--
-- Name: acces  acces _pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."acces "
    ADD CONSTRAINT "acces _pkey" PRIMARY KEY ("id_acces ");


--
-- Name: curso curso_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.curso
    ADD CONSTRAINT curso_pkey PRIMARY KEY (id_curso);


--
-- Name: document document_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (document_id);


--
-- Name: storage_control  storage_control _pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."storage_control "
    ADD CONSTRAINT "storage_control _pkey" PRIMARY KEY (storage_id);


--
-- Name: type_doc type_doc_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.type_doc
    ADD CONSTRAINT type_doc_pkey PRIMARY KEY (type_id);


--
-- Name: type_user type_user_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.type_user
    ADD CONSTRAINT type_user_pkey PRIMARY KEY (id_type);


--
-- Name: user user_id; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_id PRIMARY KEY (user_id);


--
-- Name: fki_id_curso; Type: INDEX; Schema: public; Owner: hector
--

CREATE INDEX fki_id_curso ON public."acces " USING btree (id_curso);


--
-- Name: storage_control  document_id; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."storage_control "
    ADD CONSTRAINT document_id FOREIGN KEY (document_id) REFERENCES public.document(document_id);


--
-- Name: document document_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.type_doc(type_id);


--
-- Name: acces  id_curso; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."acces "
    ADD CONSTRAINT id_curso FOREIGN KEY (id_curso) REFERENCES public.curso(id_curso);


--
-- Name: storage_control  id_curso; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."storage_control "
    ADD CONSTRAINT id_curso FOREIGN KEY (id_curso) REFERENCES public.curso(id_curso);


--
-- Name: acces  user_id ; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."acces "
    ADD CONSTRAINT "user_id " FOREIGN KEY (id_user) REFERENCES public."user"(user_id);


--
-- Name: user user_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.type_user(id_type);


--
-- PostgreSQL database dump complete
--

